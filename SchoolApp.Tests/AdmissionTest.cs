using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SchoolApp.Data;
using System;
using System.Linq;
using System.IO;
using CsvHelper;
using System.Globalization;

namespace SchoolApp.Tests
{
    public class AdmissionTest
    {
        DbContextOptions<AppDBContext> configOptions = null;
        private Admission admission;

        [SetUp]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
 
            configOptions = new DbContextOptionsBuilder<AppDBContext>().UseSqlite(connection).EnableSensitiveDataLogging().Options;

            admission = new Admission(configOptions);

        }        

        [Test]
        public void hasVacancy_True()
        {
            using (var context = new AppDBContext(configOptions))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                context.SchoolClasses.Add( new SchoolClass{ Id = 1, Standard = "6", Section = "B", MaxNumStudents = 10 });

                context.SaveChanges();

            }

            var hasVacancy = admission.hasVacancy("6");

            Assert.AreEqual(true, hasVacancy);

        }

        [Test]
        public void hasVacancy_False()
        {
            using (var context = new AppDBContext(configOptions))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                var schoolClass6B = new SchoolClass{ Id = 1, Standard = "6", Section = "B", MaxNumStudents = 5 };
                context.SchoolClasses.Add(schoolClass6B );
                context.Students.Add( new Student { Id = 1, Name = "a", schoolClass = schoolClass6B});
                context.Students.Add( new Student { Id = 2, Name = "b", schoolClass = schoolClass6B});
                context.Students.Add( new Student { Id = 3, Name = "c", schoolClass = schoolClass6B});
                context.Students.Add( new Student { Id = 4, Name = "d", schoolClass = schoolClass6B});
                context.Students.Add( new Student { Id = 5, Name = "e", schoolClass = schoolClass6B});

                context.SaveChanges();

            }

            var hasVacancy = admission.hasVacancy("6");

            Assert.AreEqual(false, hasVacancy);

        }

    }

    
}