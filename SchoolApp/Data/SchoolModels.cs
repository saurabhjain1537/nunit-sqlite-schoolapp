using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolApp.Data
{
    [Table("SCHOOL_CLASS")]
    public class SchoolClass
    {
        [Column("CLASS_ID")]
        public int Id {get; set; }
        [Column("STANDARD")]
        public string Standard { get; set; }
        [Column("SECTION")]
        public string Section { get; set; }
        [Column("STREAM")]
        public string Stream { get; set; }
        [Column("MAX_NUM_STUDENTS")]
        public int MaxNumStudents { get; set; }

        public List<Student> Students { get; set; }

    }    

    [Table("STUDENT")]
    public class Student
    {
        [Column("STUDENT_ID")]
        public int Id { get; set; }
        [Column("Name")]
        public string Name { get; set; }

        [ForeignKey("CLASS_ID")]
        public SchoolClass schoolClass { get; set;}

    }     
}