using Microsoft.EntityFrameworkCore;

namespace SchoolApp.Data
{
    public class AppDBContext:DbContext
    {
        public DbSet<SchoolClass> SchoolClasses { get; set;}
        public DbSet<Student> Students {get; set;}

        public AppDBContext() : base()
        {
        }        

        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseOracle("TODO - connection string");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SchoolClass>(s =>
                {
                    s.HasKey(k => k.Id);
                });

            modelBuilder.Entity<Student>(s =>
                {
                    s.HasKey(k => k.Id);
                });
        }

    }
}