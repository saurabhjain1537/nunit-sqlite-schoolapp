using Microsoft.EntityFrameworkCore;
using System.Linq;
using SchoolApp.Data;

namespace SchoolApp
{
    public class Admission
    {
        private DbContextOptions<AppDBContext> appDBOptions;

        public Admission()
        {
            this.appDBOptions = null;
        }

        public Admission(DbContextOptions<AppDBContext> appDBOptions)
        {
            //This constructor is used for Unit testing
            this.appDBOptions = appDBOptions;
        }

        private AppDBContext getAppDBContext()
        {
            if(this.appDBOptions == null)
            {
                return new AppDBContext();
            }
            else
            {
                return new AppDBContext(this.appDBOptions);
            }

        }

        public bool hasVacancy(string Standard)
        {
            bool hasVacancy = false;

            using(var context = getAppDBContext())
            {
                hasVacancy = context.SchoolClasses
                                        .Where( c => c.Standard == Standard && c.MaxNumStudents > c.Students.Count)
                                        .Count()
                              > 0;

            }

            return hasVacancy;
            
        }

    }
}
